#!/usr/bin/env node

const prompts = require('/usr/share/web2desk/node_modules/prompts');
const fs = require('fs');
const path = require('path');

const sh = (cmd) => { return new Promise((resolve, reject) => {
  require('child_process').exec(cmd, (err, stdout, stderr) => {
  if (err) { reject(err); }
  else { resolve(stdout); }
  });
})};

let exists = false;

(async () => {
  const Prompts = [
    {
      type: 'text',
      name: 'name',
      message: 'Name'
    },
    {
      type: 'text',
      name: 'url',
      message: 'URL'
    }
  ];

  const response = await prompts(Prompts);

  if (!response.name || !response.url) {
    process.exit(0);
  }

  const config = {
    name: response.name,
    url: response.url
  };

  const data = JSON.stringify(config, null, 2);

  const homeDir = await sh('echo -n $HOME');
  const baseDir = path.join(homeDir, '.local/share/WebApps/baseDir');
  const webAppsDir = path.join(homeDir, '.local/share/WebApps');
  const appDir = path.join(webAppsDir, response.name);
  const binDir = '/usr/local/bin';
  const appBin = path.join(binDir, response.name);
  const configFile = path.join(appDir, 'config.json');
  const pJson = path.join(appDir, 'package.json');

  const packageJson = {
    name: response.name,
    productName: response.name,
    version: "0.1.0",
    main: "main.js",
    scripts: {
      start: "../node_modules/electron/dist/electron ."
    },
    author: "Connor Etherington <connor@concise.cc>",
    license: "MIT/X Consortium",
    keywords: []
  };

  const MAIN = `
const { app, BrowserWindow, Menu, session } = require('electron');
const path = require('path');
const fs = require('fs');
const cookiesFile = path.join(__dirname, 'cookies.json');

let { name, url } = require('./config.json');

name = name.slice(0, 1).toUpperCase() + name.slice(1);

const mkMainWin = () => {
  const mWin = new BrowserWindow({
    title: name,
    width: 800,
    height: 600
  })

  session.defaultSession.on('will-download', (event, item, webContents) => {
    event.preventDefault()
    require('got')(item.getURL()).then((response) => {
      require('fs').writeFileSync(cookiesFile, response.body);
    })
  })
  mWin.loadURL(url);
}


const menu = [
  {
    label: name,
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click:  () => {
          BrowserWindow.getFocusedWindow().reload()
        }
      },
      {
        label: 'Clear Cache',
        accelerator: 'CmdOrCtrl+Shift+R',
        click:  () => {
          BrowserWindow.getFocusedWindow().webContents.session.clearCache(() => {})
        }
      },
      {
        label: 'Clear Cookies',
        accelerator: 'CmdOrCtrl+Shift+C',
        click:  () => {
          BrowserWindow.getFocusedWindow().webContents.session.clearStorageData(() => {})
        }
      },
      {
        label: 'Quit',
        accelerator: 'CmdOrCtrl+Q',
        click:  () => {
          app.quit()
        }
      }
    ]
  }
]

app.on('ready', () => {
  mkMainWin()
  Menu.setApplicationMenu(Menu.buildFromTemplate(menu))
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    mkMainWin()
  }
})

app.on('before-quit', () => {
  session.defaultSession.cookies.get({}, (error, cookies) => {
    if (error) {
      console.error(error);
    } else {
      fs.writeFileSync(cookiesFile, JSON.stringify(cookies));
    }
  });
});
`;

  if (!fs.existsSync(baseDir)) {
    fs.mkdirSync(baseDir);
  }

  if (!fs.existsSync(binDir)) {
    fs.mkdirSync(binDir);
  }

  if (!fs.existsSync(webAppsDir)) {
    fs.mkdirSync(webAppsDir);
  }

  if (fs.existsSync(appDir)) {
    exists = true;
  }

  if (!exists) {
    await fs.mkdirSync(appDir);
    await fs.writeFileSync(configFile, data);
    await fs.writeFileSync(appBin, `#!/usr/bin/env bash\n\npwdir=$PWD ; cd ${appDir} ; yarn start >/dev/null 2>&1 & disown ; cd $pwdir`);
    await fs.chmodSync(appBin, '755');
    await fs.writeFileSync(pJson, JSON.stringify(packageJson, null, 2));
    await fs.writeFileSync(path.join(appDir, 'main.js'), MAIN);
    console.log('Desktop App Created!');
  } else {
    console.log('App already exists');
  }

})();

