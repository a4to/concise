#!/usr/bin/env node

const fs = require('fs');
const fsp = require('fs').promises;
const path = require('path');
const scriptDir = path.dirname(require.main.filename);
const tesseract = require(path.join(scriptDir, 'tesseract.js'));
const { spawn } = require('child_process');

const green = '\x1b[32m';
const reset = '\x1b[0m';

const config = { lang: 'eng', oem: 1, psm: 3 };
const { exec } = require('child_process');

const sh = (cmd, opts) => new Promise((resolve, reject) => {
  exec(cmd, opts, (err, stdout, stderr) => {
    if (err) {
      reject(err);
    } else {
      resolve(stdout.trim());
    }
  });
});

let fail = false;

const ocr = async (imagePath) => {
  const Type = await sh(`file -b --mime-type ${imagePath}`);
  if(Type === 'PNG image data' || Type === 'JPEG image data' || Type === 'TIFF image data' || Type === 'image/x-portable-bitmap' || Type === 'image/png' || Type === 'image/jpeg' || Type === 'image/tiff' || Type === 'PC bitmap') {

    let binary = require('child_process').exec('which tesseract', (err, stdout, stderr) => {
      if (err) {
        console.error('\n\x1b[31m[-]\x1b[0m Fatal Error: Tesseract not found...');
        process.exit(1);
      }
      if (stdout === '') {
        console.error('\n\x1b[31m[-]\x1b[0m Fatal Error: Tesseract not found...');
        process.exit(1);
      }
      if (stderr) {
        console.error(stderr);
        process.exit(1);
      }
    });

    const image = await fsp.readFile(imagePath);
    return await tesseract.recognize(image, config);

  } else if (Type === 'PDF document') {

    let binary = require('child_process').exec('which tesseract', (err, stdout, stderr) => {
      if (err) {
        console.error('\n\x1b[31m[-]\x1b[0m Fatal Error: Tesseract not found...');
        process.exit(1);
      }
      if (stdout === '') {
        console.error('\n\x1b[31m[-]\x1b[0m Fatal Error: Tesseract not found...');
        process.exit(1);
      }
      if (stderr) {
        console.error(stderr);
        process.exit(1);
      }
    });

    await sh(`convert ${imagePath} -background white ${imagePath}.png`);
    const res = await tesseract.recognize(`${imagePath}.png`, config);
    await fs.unlinkSync(`${imagePath}.png`);
    return res;
  } else {
    try {
      return await fsp.readFile(imagePath, 'utf8');
    } catch (err) {
      fail = true;
      throw new Error('Unknown image type');
    }
  }
}

const args = process.argv.slice(2);
const writeFile = (file, data) => fs.writeFile(file, data, (err) => { if (err) console.error(err); });
const timestamp = () => new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'long', year: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit' });
const extractsDir = '/home/nvx1/.local/share/logs/ocrListener/extracts';


(async () => {
  const texts = await Promise.all(args.map(async arg => {
    const imagePath = path.resolve(process.cwd(), arg);
    try {
      return await ocr(imagePath);
    } catch (err) {
      console.error(err);
      return '';
    }
  }));

  const combinedText = texts.join('\n');

  const echo = spawn('echo', [combinedText]);
  const xclip = spawn('xclip', ['-selection', 'clipboard']);

  new Promise((resolve, reject) => {
    if (!fs.existsSync(extractsDir)) {
      fs.mkdirSync(extractsDir);
    }
    resolve(writeFile(path.join(extractsDir, `${timestamp().replace(/:/g, '-').replace(/ /g, '_')}.txt`), combinedText));
  }).then(() => {

    if (!fail) {
      console.log(`\n${green}[+] ${reset}LFP OCR:\n`);

      echo.stdout.pipe(xclip.stdin);

      echo.on('close', () => {
        console.log(combinedText);
        process.exit(0);
      });
    } else process.exit(1);
  })

})();

