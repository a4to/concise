# Saywoof - BARK Text-to-Speech Wrapper

### Welcome to Saywoof, an easy to use wrapper for the BARK text-to-speech engine. The library provides a simple set of functions to quickly get started with Bark TTS.

*Version: 0.1.0*


## Installation:

### **PyPI**:
    pip install saywoof

### **Conda**:
    conda install -c concise saywoof

### **From Source**:
#### If you prefer to install from source, clone this repo, cd into it and run:
    pip install -e .

    # or python3 ./setup.py install


# Usage

## Options:
+ -h, --help: Show help message and exit.
+ -l, --local: Load local model.
+ -L, --large: Load large model.
+ -s, --save: Save model.
+ -t, --text: Text to convert to speech.
+ -f, --file: File to convert to speech.
+ -o, --output: Output file name.
+ -r, --rate: Sample rate of output file.
+ -p, --persistent: Persistent mode.


## License
Saywoof is open source and licenced under the MIT/X Consortium license

## Support
If you encounter any problems or have suggestions for Saywoof, please open an issue on GitLab. We value your feedback and will respond as quickly as possible.


