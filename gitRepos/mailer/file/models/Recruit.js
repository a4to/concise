
const mongoose = require('mongoose');
const RecruitSchema = new mongoose.Schema({
  name: { type: String },
  preName: { type: String },
  notes: { type: String },
  surname: { type: String },
  uId: { type: String },
  recruited: { type: String },
  referals: { type: String },
  isAgency: { type: Boolean, default: false },
  recruiter: { type: String },
  ref: { type: String },
  sector: { type: String },
  email: { type: String },
  ref: { type: String },
  fullRef: { type: String },
  link: { type: String },
  contactno: { type: String },
  username: { type: String },
  password: { type: String },
  status: { type: String },
  emailUser: { type: String },
  bank: { type: String },
  accountHolder: { type: String },
  accountNo: { type: String },
  superNo: { type: String },
  ownsSupersim: { type: String },
  passedTest: { type: Boolean },
  date: { type: Date, default: new Date().toLocaleString() },
  submitted: { type: Boolean, default: false },
  agency: { type: String },
});

const Recruit = mongoose.model('Recruit', RecruitSchema);

module.exports = Recruit
