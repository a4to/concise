
const mongoose = require('mongoose');

const PaymentSchema = new mongoose.Schema({
  transaction: { type: Object },
  msisdn: { type: String },
  site: { type: String },
  date: { type: Date, default: new Date() },
  email: { type: String },
});

const Payment = mongoose.model('Payment', PaymentSchema);

module.exports = Payment
