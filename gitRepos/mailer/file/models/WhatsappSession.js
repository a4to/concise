
const mongoose = require('mongoose');

const WhatsappSessionSchema = new mongoose.Schema({
  nr: { type: String },
  messages: [ { type: String } ],
  last: { type: String },
});

const WhatsappSession = mongoose.model('WhatsappSession', WhatsappSessionSchema);

module.exports = WhatsappSession

