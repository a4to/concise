const mongoose = require('mongoose');

const amailVerificationSchema = new mongoose.Schema({
  agentId: String,
  uniqueId: String,
  createdAt: Date,
  expiresAt: Date
});

const amailVerification = mongoose.model('amailVerification', amailVerificationSchema);

module.exports = amailVerification;
