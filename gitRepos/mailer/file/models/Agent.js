
const mongoose = require('mongoose');
const AgentSchema = new mongoose.Schema({
  name: { type: String },
  surname: { type: String },
  recruited: { type: String },
  referals: { type: String },
  email: { type: String },
  userAcc: { type: String },
  ref: { type: String },
  code: { type: String },
  division: { type: String },
  isAgency: { type: Boolean, default: false },
  fullRef: { type: String },
  sector: { type: String },
  link: { type: String },
  contactno: { type: String },
  username: { type: String },
  password: { type: String },
  token: { type: String },
  verified: { type: Boolean, default: false },
  status: { type: String },
  emailUser: { type: String },
  bank: { type: String },
  accountHolder: { type: String },
  rank: { type: String },
  agency: { type: String },
  points: { type: String },
  uId: { type: String, default: 'NA' },
  accountNo: { type: String },
  superNo: { type: String },
  passedTest: { type: Boolean, default: false },
  ownsSupersim: { type: String },
  signupNotice: { type: Boolean },
  subscribers: {
      type: [
          {
              name: { type: String },
              surname: { type: String },
              email: { type: String },
              contactno: { type: String },
              signedUp: { type: String, default: new Date() },
              id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
          }
      ],
      default: []
  },
  date: { type: Date, default: new Date().toLocaleString() },
  lastLogin: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' }) + ', ' + new Date().toLocaleTimeString('en-GB', { hour: '2-digit', minute: '2-digit' }).replace(/:/g, 'h') }
      }
    }
  ],
  last2Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },

        date: { type: String, default: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' }) + ', ' + new Date().toLocaleTimeString('en-GB', { hour: '2-digit', minute: '2-digit' }).replace(/:/g, 'h') }
      }
    }
  ],
  last3Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' }) + ', ' + new Date().toLocaleTimeString('en-GB', { hour: '2-digit', minute: '2-digit' }).replace(/:/g, 'h') }
      }
    }
  ],
  last4Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' }) + ', ' + new Date().toLocaleTimeString('en-GB', { hour: '2-digit', minute: '2-digit' }).replace(/:/g, 'h') }
      }
    }
  ],
  last5Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' }) + ', ' + new Date().toLocaleTimeString('en-GB', { hour: '2-digit', minute: '2-digit' }).replace(/:/g, 'h') }
      }
    }
  ],
  lastLogins: {
    type: [String],
    default: []
  },
});

const Agent = mongoose.model('Agent', AgentSchema);

module.exports = Agent
