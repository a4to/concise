const mongoose = require('mongoose');
const ans = () => {

  const date = new Date(Date.now());
  const DTE = new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' })
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate().toString().padStart(2, '0');
  const hour = date.getHours().toString().padStart(2, '0');
  const minute = date.getMinutes().toString().padStart(2, '0');

  const ans = `${DTE}, ${hour}h${minute}`;
  return ans;

}

const date = ans();

const anss = () => {
  const date = new Date(Date.now());
  date.setMonth(date.getMonth());
  const year = date.getFullYear();
  const month = date.toLocaleDateString('en-GB', {  month: 'long' }).substr(0, 3);
  const day = date.getDate().toString()
	return `${day} ${month} ${year}`;
}

const abdate = anss();

const loginDate = abdate + ', ' + new Date(Date.now()).getHours().toString().padStart(2, 0) + 'h' + new Date(Date.now()).getMinutes().toString().padStart(2, 0);


const UserSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  name: {
    type: String,
  },
  title: {
    type: String,
  },
  payUuid: {
    type: String,
  },
  surname: {
    type: String,
  },
  invName: {
    type: String,
  },
  isAffiliate: {
    type: Boolean,
    default: false
  },
  affId: {
    type: String,
    default: 'NA'
  },
  regIP: {
    type: String,
  },
  ips: {
    type: Array,
  },
  invSurname: {
    type: String,
  },
  invEmail: {
    type: String,
  },
  companyName: {
    type: String,
  },
  invTitle: {
    type: String,
  },
  contactno: {
    type: String,
  },
  location: {
    type: Object,
  },
  estimateLocations: {
    type: [Object],
  },
  studentno: {
    type: String,
  },
  email: {
    type: String,
  },
  firstSim: {
    type: Boolean,
    default: true,
  },
  password: {
    type: String,
  },
  payLink: {
    type: String,
  },
  photo: {
    type: String,
    default: '/img/default/user.png'
  },
  agent: {
    type: String,
    required: true,
  },
  firstLogin: {
    type: Boolean,
    default: true,
  },
  verified: {
    type: Boolean,
    default: false,
    required: true
  },
  welcomed: {
    type: Boolean,
    default: false,
  },
  invoiceId: {
    type: String,
    required: true,
    default: 'NA'
  },
  google: {
    type: Boolean,
    default: false
  },
  googleId: {
    type: String
  },
  googleEmail: {
    type: String
  },
  googleName: {
    type: String
  },
  emails: {
    type: String,
  },
  agency: {
    type: String,
  },
  payments: {
    type: [Object],
  },
  invoices: {
    type: [String],
  },
  date: {
    type: String,
    default: date
  },
  address: {
    type: String,
  },
  address2: {
    type: String,
  },
  city: {
    type: String,
  },
  province: {
    type: String,
  },
  postalCode: {
    type: String,
  },
  saveInfo: {
    type: Boolean,
  },
  lastLogin: {
    type: Object,
  },
  last2Login: {
    type: Object,
  },
  last3Login: {
    type: Object,
  },
  last4Login: {
    type: Object,
  },
  last5Login: {
    type: Object,
  },
  lastLogins: {
    type: [String],
    default: []
  },
  resetPasswordToken: {
    type: String,
  },
  rating: {
    type: String,
    default: 'NA'
  },
  resetPasswordExpires: {
    type: Date,
  },
  success: {
    type: Boolean,
    default: false,
  },
  sims: {
    type: [
      {
        number: { type: String },
        MSISDN: { type: String },
        simId: { type: String },
        packageSize: { type: Number },
        remainingData: { type: Number },
        usageMtd: { type: Number },
        paymentStatus: { type: String },
        paymentDate: { type: String },
        startDate: { type: String, default: abdate },
        active: { type: Boolean, default: false },
        billed: { type: String, },
        nickname: { type: String, default: 'NA' },
        message: { type: String },
        currentAllocation: { type: Number },
        notified: { type: Boolean, default: false },
        previousAllocation: { type: Number  },
        usageHistory: { type: Array, default: [] },
        status: { type: String }
      }
    ],
    default: []
  },
  simCount: {
    type: Number,
    default: 0
  },
  dailyUpdates: {
    type: Boolean,
    default: false
  },
  monthlyInvoices: {
    type: Boolean,
    default: false
  },
  reminderDay: {
    type: String
  },
  inPayment: {
    type: Object,
    default: {}
  },
  signedUp: {
    type: Date,
    default: new Date()
  },
  payLink: {
    type: String
  },
  payLinks: {
    type: [
      {
        link: { type: String },
        invoiceId: { type: String },
        amount: { type: Number },
        type: { type: String },
        description: { type: String },
        tokenId: { type: String },
        status: { type: String, default: 'Pending' },
        MSISDN: { type: String },
        nickname: { type: String },
        qr: { type: String },
        date: { type: String, default: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' }) + ', ' + new Date().toLocaleTimeString('en-GB', { hour: '2-digit', minute: '2-digit' }).replace(/:/g, 'h') }
      },
    ],
    default: []
  },
  monthlyPayLink: {
    type: String
  },
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
