const mongoose = require('mongoose');

const date = () => {

  let days = { Monday: 1, Tuesday: 2, Wednesday: 3, Thursday: 4, Friday: 5, Saturday: 6, Sunday: 0 };
  let now = new Date();
  let weekDay = now.getDay().toString();
  let longDay = Object.keys(days).find(key => days[key] == weekDay);

  const hour = now.getHours().toString().padStart(2, '0');
  const minute = now.getMinutes().toString().padStart(2, '0');
  const date = `${longDay} ${hour}:${minute} ${hour > 12 ? 'pm' : 'am'}`;

  return date

}

const FeedbackSchema = new mongoose.Schema({
  rating: { type: String },
  comment: { type: String },
  name: { type: String },
  date: { type: String, default: date() },
});

const Feedback = mongoose.model('Feedback', FeedbackSchema);

module.exports = Feedback
