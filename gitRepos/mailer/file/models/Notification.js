const mongoose = require('mongoose');


const NotificationSchema = new mongoose.Schema({
  data: { type: Object },
});

const Notification = mongoose.model('Notification', NotificationSchema);

module.exports = Notification

