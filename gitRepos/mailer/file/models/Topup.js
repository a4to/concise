const mongoose = require('mongoose');

const TopUpSchema = new mongoose.Schema({
  msisdn: { type: String },
  size: { type: Number },
  response: { type: Object },
  email: { type: String },
  date: { type: Date, default: new Date().toLocaleDateString() }
});

const TopUp = mongoose.model('TopUp', TopUpSchema);

module.exports = TopUp

