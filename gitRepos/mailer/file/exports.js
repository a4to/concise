
const fs = require('fs');
const path = require('path');
const mailDir = path.join(__dirname, 'templates');

const getmail = (mailName) => {
  const mail = require(`${mailDir}/${mailName}`);
  return mail;
}

const getmails = () => {
  let mails = {};
  const files = fs.readdirSync(mailDir);
  files.forEach(mail => {
    if(mail !== 'index.js' && mail !== 'exports.js') {
      const mailName = mail.split('.')[0];
      mails[mailName] = getmail(mail);
    }
  });
  return mails;
}

const getSubject = (mailName, company) => {
  const { subject } = require(`${mailDir}/${mailName}`);
  return subject(company);
}

const mails = getmails();
mails['getSubject'] = getSubject;

module.exports = mails;

