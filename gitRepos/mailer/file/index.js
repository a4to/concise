
const mailType = require('./exports');
const nodemailer = require('nodemailer');
const conf = require('./.config/mailKeys.json');
const User = require('./models/User');
const Agency = require('./models/Agency');
const Affiliate = require('./models/Affiliate');
require('colors');

let senders = [];

conf.forEach((sender) => {
  senders.push({account: sender.account, from: sender.from, transporter: nodemailer.createTransport(sender.transporter)});
});

const sender = senders[0];

/* ======================================================================================================== */

const mail = async (array) => {
const line = `====================================================================================================`;
  const { from, to, subject, html, transporter } = array;
    const info = await transporter.sendMail({ from, to, subject, html })
    .then((info) => {
      var data = '\n';
      for (var i in Object.keys(info)) {
        data += Object.keys(info)[i] + ': ' + Object.values(info)[i] + '\n';
      }
    const Info = `\n[+]`.green + ` Mail sent to ` + `${to}`.cyan + `\n\n${line.magenta}\n                                                ` + `INFO`.yellow + `\n${line.magenta}\n\n${data}\n\n` + `${line.magenta}\n\n`
    console.log(Info);
    return Info;
  }).catch((err) => {
    console.log(err);
  });
}


const sendMail = async (obj) => {

  const Agencies = await Agency.find();
  const Affiliates = await Affiliate.find();
  const Users = await User.find();

  const { user, type } = obj;
  let getHtml, getSubject;

  if(mailType[type]) {
    getHtml = mailType[type].html;
    getSubject = mailType[type].subject;
  } else return `Error: Mail type ${type} not found.`;

  let link;

  if(obj.link) link = obj.link;
  else link = 'NA';

  const agency = Agencies.find((agency) => agency.domainName == user.agency.replace('https://', '').replace('http://', ''));
  const baseUrl = `https://${agency.domainName}`;
  const tradeName = agency.tradeName;
  const legalName = agency.legalName;
  const logo = baseUrl + `/img/logos/${agency.domainName}.png`;
  const from = tradeName + ' <' + sender.account + '>';
  const { name, surname, email } = user;
  const to = user.email;

  const html = getHtml({name, surname, link, baseUrl, tradeName, legalName, logo});
  const subject = getSubject(tradeName);
  const transporter = sender.transporter;
  const arr = { from, to, subject, html, transporter };

  await mail(arr).then((info) => {
    return info;
  }).catch((err) => {
    console.log(err);
  });

};

module.exports = sendMail;

