#!/usr/bin/env bash

MarkDir="${XDG_CONFIG_HOME:-$HOME/.config}/qed"
MarkFile="${MarkDir}/markfile"

[ ! -d ${MarkDir} ] && mkdir -p ${MarkDir}
[ ! -f ${MarkFile} ] && touch ${MarkFile}


getSlot(){
  slot=$(dialog --stdout \
    --backtitle "Markfile" \
    --title "Markfile" \
    --menu "Select MarkFile Slot:" \
    0 0 0  "${markfileOpts[@]}" \
  )
}

newFileEdit(){

  markfile=$1

  markfileOpts=(
    1   "$(grep '^1=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    2   "$(grep '^2=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    3   "$(grep '^3=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    4   "$(grep '^4=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    5   "$(grep '^5=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    6   "$(grep '^6=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    7   "$(grep '^7=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    8   "$(grep '^8=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    9   "$(grep '^9=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
    10  "$(grep '^10=' $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev || echo 'Empty')"
  )


  [[ -d $markfile ]] && echo -e "\n\e[31;1m[-] Error: $markfile is a directory, not a file!\e[0m" && exit 1

  [[ ! -f $markfile ]] && echo -e "\n\e[31;1m[-] File does not exist!\e[0m\n" && exit 1

  [[ -f $markfile ]] && markfile=`realpath $markfile` && getSlot

  [[ -z $slot ]] && echo -e "\n\e[31;1m[-] No slot selected!\e[0m\n" && exit 1

  [[ -n  $slot ]] && grep -q "^$slot=" $MarkFile &&
    sed -i "s|^$slot=.*|$slot=$markfile|" $MarkFile ||
    echo "$slot=$markfile" >> $MarkFile &&
    clear
    echo -e "\n\n\e[32;1m[+] `basename $markfile` set to slot $slot\e[0m ✅\n" ||
    echo -e "\n\n\e[31;1m[-] Error setting slot $slot\e[0m\n"

}

USAGE(){
  clear && echo -e "\n\n  \e[32;1;3mUsage:\e[0;35;1m\n\n
  $(basename $0) \e[1;37m            => \e[36;1m (Dry Run) List all slots and associated files\e[35;1m\n
  $(basename $0) \e[33;1m [  FILE  ]\e[1;37m => \e[36;1m Save file to a selected slot (1-10)\e[35;1m
  $(basename $0) \e[33;1m [ 1 - 10 ]\e[1;37m => \e[36;1m Open file in slot selected slot in ${EDITOR:-your editor}\e[35;1m
  $(basename $0) \e[33;1m [ -c|cls ]\e[1;37m => \e[36;1m Clear all slots\e[0m\n\n"
}

case $1 in
  1) ${EDITOR:-nvim} $(grep "^1=" $MarkFile | cut -d= -f2) ;;
  2) ${EDITOR:-nvim} $(grep "^2=" $MarkFile | cut -d= -f2) ;;
  3) ${EDITOR:-nvim} $(grep "^3=" $MarkFile | cut -d= -f2) ;;
  4) ${EDITOR:-nvim} $(grep "^4=" $MarkFile | cut -d= -f2) ;;
  5) ${EDITOR:-nvim} $(grep "^5=" $MarkFile | cut -d= -f2) ;;
  6) ${EDITOR:-nvim} $(grep "^6=" $MarkFile | cut -d= -f2) ;;
  7) ${EDITOR:-nvim} $(grep "^7=" $MarkFile | cut -d= -f2) ;;
  8) ${EDITOR:-nvim} $(grep "^8=" $MarkFile | cut -d= -f2) ;;
  9) ${EDITOR:-nvim} $(grep "^9=" $MarkFile | cut -d= -f2) ;;
  10) ${EDITOR:-nvim} $(grep "^10=" $MarkFile | cut -d= -f2) ;;
  CLEAR|CLEAN|cls) echo -n "" > $MarkFile && echo -e "\n\e[32;1m[+] Markfile Cleared\e[0m ✅\n" ;;
  -h|--help|help) USAGE ;;
  "") while read -r line; do
        echo -e "\e[32;1m[+] Slot $line\e[0m: $(grep "^$line=" $MarkFile | cut -d= -f2 | rev | cut -d/ -f1 | rev)"
      done < <(seq 1 10) ;;
  *) newFileEdit $1 ;;
esac

