/* Bar functionality */
#include "functions.c"
#include "bar_ltsymbol.c"
#include "bar_status.c"
#include "bar_tags.c"
#include "bar_wintitle.c"
#include "cust/bar_time.c"
#include "cust/bar_datetime.c"
#include "cust/bar_date.c"
