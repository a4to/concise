

#define SH(cmd) (const char*[]){ "/bin/sh", "-c", cmd, NULL }
#define ROFI() (const char*[]){ "/bin/rofi", "-show", "run", NULL }
#define TERM_RUN(cmd) (const char*[]){ "/bin/alacritty", "-e", cmd, NULL }

void get_time(char *time) {
    FILE *fp;
    char buf[100];
    fp = popen("echo -en \" `date +%H:%M`\"", "r");
    fgets(buf, 100, fp);
    pclose(fp);
    strcpy(time, buf);
}

int
width_time(Bar *bar, BarWidthArg *a)
{
  return TEXTW("00:00") + lrpad;
}

int
draw_time(Bar *bar, BarDrawArg *a)
{
	int boxs = drw->fonts->h / 9;
	int boxw = drw->fonts->h / 6 + 2;
	int x = a->x, w = a->w;
	Monitor *m = bar->mon;

	drw_setscheme(drw, scheme[m == selmon ? SchemeActive : SchemeInactive]);

  char time[100];
  get_time(time);
  return drw_text(drw, x, 0, w, bh, lrpad / 2, time, 0);
}

int
click_time(Bar *bar, Arg *arg, BarClickArg *a)
{
  termrun("ttyclk");
  return EXIT_SUCCESS;
}

int clickk_time(Bar *bar, Arg *arg, BarClickArg *a)
{
  termrun("gbmonit");
  return EXIT_SUCCESS;
}

