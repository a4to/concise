# PMAN - A minimal terminal-based download manager.

This Script is intended to be used as a donload helper for all major package managers,
as well as for obtaining Torrents and files from URLs.

## Usage

Run `pman` - you will be taken to a file where you can enter the package names and sources that you want to download.

**Other use cases:**

`pman` -f \<file with list of packages>\
`pman` \<package1> \<package2> \<package3>\
`pman` MH,<repo>\
`pman` L,\<gitlab username>/\<repo>\
`pman` H,\<github username>/\<repo>

**Optionally, you can put a comment, in quotes, following the package name and an additional comma, to replace the description.**\
**For example:**

`pman` \<package name>,"\<comment>"\
`pman` MH,"\<description>"


## List Formatting

**Do not enter any spaces between package names, or between package the package source and the package name (the comma).**


### The following list is provided below for reference:


    Local or AUR Packages  :     <packageName>
    
    The method used above is likely to be used in most cases.
    
    GitHub Repositories    :     H,<Creator/HubRepoName>
    GitLab Repositories    :     L,<Creator/LabRepoName>
    Personal GitHub        :     PH,<yourGitHubRepoName>
    Personal GitLab        :     PL,<yourGitLabRepoName>
    Snap Packages          :     S,<snapPackageName>
    Python Pip Packages    :     P,<pythonPackageName>
    NPM or Nodejs Packages :     N,<packageName>
    Web files or Torrents  :     W,<URL>


### List Formatting Examples:
  
    packages=(
    
    zsh
    zsh,"A very advanced and programmable command interpreter (shell) for UNIX"
    P,pipenv,"A comment is not necessary and purely meant as eye candy if saving the script for later use"
    H,neovim/neovim
    L,a4to/auto
    MH,auto
    S,snap-store
    N,lite-server

     )

