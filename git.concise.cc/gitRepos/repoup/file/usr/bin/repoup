#!/usr/bin/env bash

# Debugging:
#trap 'lastCommand=$currentCommand; currentCommand=$BASH_COMMAND' DEBUG
#trap 'echo -e "\n\e[1;31m [-]\e[0m\e[1;33m Last Successful Command:\e[0m $lastCommand\e[0m\
#  \n\e[1;31m [-]\e[0m\e[1;33m Failed Command:\e[0m $BASH_COMMAND \e[0m\n"' EXIT QUIT STOP ERR


ADD(){
  for x in ${@}; do
    cp -vfr ${PWDIR}/${x} ${REPOPATH} >/dev/null 2>&1 || 
    mkdir -p ${REPOPATH} && cp -vfr ${PWDIR}/${x} ${REPOPATH} >/dev/null 2>&1 &&
    echo -e "\e[1;32m\n[+] \e[0m\e[1;33m${x}\e[0m \e[1;32madded to \e[36m$(basename ${REPO})\e[32m on \e[36m${BRANCH:-master}\e[0m\n"
  done
}

PUSH(){
  for x in ${BRANCHES[@]}; do
    cd $REPO
    git checkout $x
    git add .
    git commit -m "$REPO update"
    git push origin ${x:-master}
  done
}

PUSHTOBRANCH(){
  BRANCH=${1:-master}
  cd $REPO ;
  git checkout $1
  git add .
  git commit -m "$REPO update"
  git push origin $1
}

ADDTOALL(){
  for x in ${BRANCHES[@]}; do
    BRANCH=${x:-master}
    cd $REPO
    git checkout $x
    ADD ${*}
    git add .
    git commit -m "$REPO update, added ${*}"
  done
}

ADDTOBRANCH(){
  BRANCH=${1:-master}
  cd $REPO
  git checkout $1
  ADD ${@:2}
  git add .
  git commit -m "$REPO update, added ${@:2}"
}

USAGE(){
echo -e "\n\n  \e[1;32mUsage: \e[0;33m`echo $0|sed 's|./||'` \e[0;35m<FLAG>

    \e[0;35m-b  \e[0;36m <BRANCH> <FILES> :  \e[0;37mAdd file(s) to specified branch
    \e[0;35m-a  \e[0;36m <BRANCH> <FILES> :  \e[0;37mAdd file(s) to all branches
    \e[0;35m-pb  \e[0;36m<BRANCH>         :  \e[0;37mPush to specified branch
    \e[0;35m-pa  \e[0;36m<BRANCH>         :  \e[0;37mPush to all branches
    \e[0;35m-h   \e[0;36m<REPO>           :  \e[0;37mDisplay this help message
    \e[0;35m-r   \e[0;36m<REPO>           :  \e[0;37mSpecify the repo (this flag must be first, followed by the repo path, then additional options)

    
    \e[0;37mBy default, files are added to the current branch.

    \e[1;33mExamples : \e[0;37m`echo $0|sed 's|./||'` -r /path/to/repo -b master <file1> <file2> <file3>
             : \e[0;37m`echo $0|sed 's|./||'` -r /path/to/repo -a <file1> <file2> <file3>
            
                
  \e[1mA Default Repo to which files are added can be set in the repouprc file, located in ${XDG_CONFIG_HOME:-$HOME/.config}/repoup/repouprc

  \e[0;33mNOTE: \e[1;34mUnless a default repo is set, the repo must be specified with the \e[0;35m-r \e[0;36m<REPO> \e[0;34mflag\e[0m\n"
}

MAIN(){

  REPOPATH="${REPO}/`pwd | sed -e "s|$HOME/||g"`"
  BRANCHES=( $(cd $REPO; echo `git branch|sed 's|\* ||'`) )

  case ${1} in
    -b|--branch) ADDTOBRANCH ${2} ${@:3} ;;
    -a|--add) ADDTOALL ${@:2} ;;
    -pb|--push) PUSHTOBRANCH ${2} ;;
    -p|-pa|--pushall) PUSH ;;
    -r) [[ -n ${2} ]] && REPO=$(realpath ${2}) ; MAIN ${@:3} ;;
    -h|help) USAGE ;;
    '') echo -e "\n\e[1;31m [-]\e[0m\e[1;33m No flags specified, use \e[0;35m-h \e[0;33mfor help\e[0m\n" ;;
    *) ADD ${@} ;;
  esac
}



 # Script Start:
# ---

set -e

PWDIR=`pwd`

source ${XDG_CONFIG_HOME:-$HOME/.config}/repoup/repouprc >/dev/null 2>&1 || true

[[ -n ${DEFAULT_REPO} ]] && REPO=${DEFAULT_REPO}


MAIN ${@} || USAGE

# ---

