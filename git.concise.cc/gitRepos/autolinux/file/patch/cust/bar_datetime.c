int
draw_datetime(Bar *bar, BarDrawArg *a)
{
  int boxs = drw->fonts->h / 9 || 0;
  int boxw = drw->fonts->h / 6 + 2 || 0;
  int x = a->x, w = a->w;
  Monitor *m = bar->mon;

  drw_setscheme(drw, scheme[m == selmon ? SchemeActive : SchemeInactive]);

  char date[100];
  get_time(date);
  date[strlen(date) - 1] = '\0';

  drw_text(drw, x, 0, w, bh, lrpad / 2, date, 0);
  return 0;
}
