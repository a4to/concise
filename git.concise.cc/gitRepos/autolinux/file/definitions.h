
#include <stdlib.h>
#include <signal.h>

#define TERMINAL "st"
#define TERMCLASS "St"
#define TERMINAL2 "alacritty"
#define TERMCLASS2 "Alacritty"
#define BROWSER "chromium"
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define BACSLASH 0x5c
#define CTRLKEY ControlMask
#define NOMOD 0

#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define HARD CTRLKEY|ALTKEY|MODKEY
#define FUZZY CTRLKEY|ALTKEY|ShiftMask
#define IMPOSSIBLE CTRLKEY|ALTKEY|MODKEY|ShiftMask

static void start_vnc();
static void stop_vnc();
static void view_vnc(const Arg *arg);

static const char *vnc_cmd[] = { "vncviewer", "-FullScreen", "-passwd", "~/.vnc/laptopPasswd", "A:6010", NULL };

void start_vnc() {
  if (system("pgrep -x vncviewer > /dev/null") != 0) {
    system("vncviewer -FullScreen -passwd ~/.vnc/laptopPasswd A:6010 &");
  }
}

void stop_vnc() {
  system("pkill -x vncviewer");
}

void view_vnc(const Arg *arg) {
  if (selmon->tagset[selmon->seltags] & (1 << 4)) {
    stop_vnc();
  } else {
    start_vnc();
  }
  view(arg);
}

