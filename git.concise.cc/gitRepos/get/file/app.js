#!/usr/bin/env node

/**
 * @license get
 * MIT/X Consortium License
 *
 * File: app.js
 * © Agentics (Pty) Ltd <connor@agentics.co.za>
 *
*/

const axios = require('axios');
const fs = require('fs');
const yargs = require('yargs');
const cheerio = require('cheerio');
const setCookieParser = require('set-cookie-parser');

let puppeteer;

const log = (input, ...args) => {
  const colors = {
    r: '[31;1m', g: '[32;1m', b: '[34;1m', y: '[33;1m', w: '[37;1m',
    m: '[35;1m', x: '[38;2;103;208;168m', c: '[36;1m', C: '[0m'
  };
  input = input.concat(':C: ', ...args).toString();
  let result = input.replace(/:([rgbyxwmcC]):/g, (match, p1) => colors[p1] || '');
  console.log(result + colors.C);
};

const { JSDOM } = require('jsdom');

class Page {
  constructor(url, html) {
    this.url = url;
    this.html = html;
    this.getDoc();
  }

  async getDoc() {
    if (!this.html) {
      const response = await fetch(this.url);
      this.html = await response.text();
    }
    const dom = new JSDOM(this.html);
    this.document = dom.window.document;
    this.window = dom.window;
    return { document: this.document, window: this.window };
  }

  static async getDoc(url) {
    const response = await fetch(url);
    const html = await response.text();
    const dom = new JSDOM(html);
    return { document: dom.window.document, window: dom.window };
  }
}

class Scraper {
  constructor(html, options) {
    this.jar = options.jar || options.cookiejar || options.cookies || [];
    this.cookies = Array.isArray(this.jar) ? this.jar : [this.jar];
    this.cookies = this.cookies.flat().join('; ');
    this.initialized = false;
    this.initPromise = this.init(html, options);
    this.headless = options.headless !== undefined ? options.headless : true;
    this.url = html.toString().startsWith('http') ? html : null;
    this.response = null;
    this.verbose = options.verbose || false;
  }

  async init(html, options) {
    if (html.toString().startsWith('http')) {
      try {
        let response;
        options.headers = options.headers || {};
        if (this.cookies.length > 0) {
          options.headers['Cookie'] = this.cookies;
        }
        response = options.method === 'POST' ? await axios.post(html, options.data, { headers: options.headers }) : await axios.get(html, { headers: options.headers });
        this.cookies = setCookieParser.parse(response.headers['set-cookie'], { map: false });
        this.$ = cheerio.load(response.data);
        this.response = response;
        if(this.verbose) log(`:g:+ :x:Fetched ${html}`);
      } catch (error) {
        log(`:r:- :x:Error making request to ${html}: ${error.message}`);
        throw error;
      }
    } else {
      this.$ = cheerio.load(html);
      if(this.verbose) log(`:g:+ :x:Loaded HTML content`);
    }
    this.initialized = true;
  }

  async ensureInitialized() {
    if (!this.initialized) {
      await this.initPromise;
    }
  }

  async scrape(selector) {
    await this.ensureInitialized();
    const result = this.$(selector).text();
    if(this.verbose) log(`:g:+ :x:Scraped content for selector ${selector}`);
    return result;
  }

  async pageText() {
    await this.ensureInitialized();
    const allText = this.$('body').html();
    const removeCSSAndJS = (text) => {
      text = text.replace(/<style[^>]*>[\s\S]*?<\/style>/gi, '');
      text = text.replace(/<script[^>]*>[\s\S]*?<\/script>/gi, '');
      text = text.replace(/\s*style=["'][^"']*["']/gi, '');
      return text;
    };
    const cleanedHTML = removeCSSAndJS(allText);
    let textContent = this.$('<div>').html(cleanedHTML).text();
    textContent = textContent.replace(/(\n\s*){3,}/g, '\n\n');
    if(this.verbose) log(`:g:+ :x:Extracted page text`);
    return textContent;
  }

  async evaluate(script) {
    await this.ensureInitialized();
    if (!puppeteer) {
      puppeteer = require('puppeteer-extra');
      const pluginStealth = require('puppeteer-extra-plugin-stealth');
      puppeteer.use(pluginStealth());
    }
    let executablePath = require('puppeteer').executablePath();
    if (!fs.existsSync(executablePath)) {
      log(`:y:? :x:Puppeteer executable not found. Attempting to install...`);
      try {
        await require('child_process').execSync('npx puppeteer browsers install');
        executablePath = require('puppeteer').executablePath();
      } catch (error) {
        log(`:r:- :x:Failed to install Puppeteer executable. Please install manually.`);
        process.exit(1);
      }
    }

    const launchOptions = { executablePath };

    const browser = await puppeteer.launch({ ...launchOptions, headless: this.headless, defaultViewport: launchOptions.defaultViewport || null });

    const page = await browser.newPage();
    if (this.cookies && this.cookies.length > 0) {
      const cookieStrings = Array.isArray(this.cookies) ? this.cookies : [this.cookies];
      const cookies = [];
      cookieStrings.forEach((cookieStr) => {
        const parsedCookies = setCookieParser.parse(cookieStr, { map: false });
        parsedCookies.forEach((parsedCookie) => {
          const cookie = {
            name: parsedCookie.name,
            value: parsedCookie.value,
            domain: parsedCookie.domain || new URL(this.url).hostname,
            path: parsedCookie.path || '/',
            expires: parsedCookie.expires
              ? Math.floor(parsedCookie.expires.getTime() / 1000)
              : undefined,
            httpOnly: parsedCookie.httpOnly,
            secure: parsedCookie.secure,
            sameSite: parsedCookie.sameSite && parsedCookie.sameSite.toLowerCase(),
          };
          cookies.push(cookie);
        });
      });
      if(this.verbose) log(`:g:+ :x:Setting cookies for ${this.url}`);
      await page.setCookie(...cookies);
    }
    await page.goto(this.url, { waitUntil: 'networkidle0' });
    if(this.verbose) log(`:g:+ :x:Navigated to ${this.url}`);
    const result = await page.evaluate(script);
    await browser.close();
    this.evalResult = result;
    if(this.verbose) log(`:g:+ :x:Evaluated script on ${this.url}`);
    return result;
  }

  async links() {
    await this.ensureInitialized();
    const links = this.$('a')
      .map((i, el) => this.$(el).attr('href'))
      .get();
    if(this.verbose) log(`:g:+ :x:Extracted links from ${this.url}`);
    return links;
  }

  async images() {
    await this.ensureInitialized();
    const images = this.$('img')
      .map((i, el) => this.$(el).attr('src'))
      .get();
    if(this.verbose) log(`:g:+ :x:Extracted images from ${this.url}`);
    return images;
  }

  async getHtml() {
    await this.ensureInitialized();
    const html = this.$.html();
    if(this.verbose) log(`:g:+ :x:Retrieved HTML content from ${this.url}`);
    return html;
  }

  async getResponse() {
    await this.ensureInitialized();
    return this.response;
  }

  async getCookies() {
    await this.ensureInitialized();
    const cookies = this.response.headers['set-cookie'];
    if(this.verbose) log(`:g:+ :x:Retrieved cookies from ${this.url}`);
    return cookies;
  }
}

if (require.main == module) {
  (async () => {
    const argv = yargs
      .option('g', {
        alias: 'get',
        type: 'boolean',
        description: 'GET request (default)',
        default: true,
      }).option('p', {
        alias: 'post',
        type: 'boolean',
        description: 'POST request',
        default: false,
      }).option('d', {
        alias: 'data',
        type: 'string',
        description: 'Data to send with POST request',
      }).option('e', {
        alias: 'eval',
        type: 'string',
        description: 'Evaluate JavaScript on the page',
      }).option('u', {
        alias: 'url',
        type: 'string',
        description: 'URL to request',
        demandOption: true,
      }).option('s', {
        alias: 'save',
        type: 'boolean',
        description: 'Save response and cookies to files',
        default: false,
      }).option('S', {
        alias: 'selector',
        type: 'string',
        description: 'Selector to scrape',
      }).option('j', {
        alias: 'jar',
        type: 'string',
        description: 'Cookie jar - string or array of strings',
        default: [],
      }).option('f', {
        alias: 'file',
        type: 'string',
        description: 'File to load cookies from',
      }).option('l', {
        alias: 'links',
        type: 'boolean',
        description: 'Return page links',
        default: false,
      }).option('t', {
        alias: 'text',
        type: 'boolean',
        description: 'Return page text',
        default: false,
      }).option('H', {
        alias: 'html',
        type: 'boolean',
        description: 'Return page HTML',
        default: false,
      }).option('b', {
        alias: 'browser',
        type: 'boolean',
        description: 'Show browser window',
        default: false,
      }).option('c', {
        alias: 'cookies',
        type: 'boolean',
        description: 'Return cookies',
        default: false,
      }).option('h', {
        alias: 'header',
        type: 'array',
        description: 'Headers for the request (can be used multiple times)',
      }).option('v', {
        alias: 'verbose',
        type: 'boolean',
        description: 'Verbose output',
        default: false,
      }).help().argv;

    const method = argv.p ? 'POST' : 'GET';
    const url = argv.u;

    const headers = argv.h ? argv.h.reduce((acc, header) => {
      const [key, value] = header.split(':');
      acc[key.trim()] = value.trim();
      return acc;
    }, {}) : {};

    if (argv.d) headers['Content-Type'] = 'application/x-www-form-urlencoded';

    let data = JSON.parse(argv.d);

    const config = { method, headers, url, withCredentials: true, data: data || {} };


    const saveFiles = (response) => {

      fs.writeFileSync('response.json', JSON.stringify({
        data: response.data,
        headers: response.headers,
        cookies: response.headers['set-cookie'] || [],
      }, null, 2));

      fs.writeFileSync('cookies.json', JSON.stringify({
        cookies: response.headers['set-cookie'] || [],
      }, null, 2));

      log(':g:+ :x:Response and cookies saved.');

    };


    if (argv.b) config.headless = false;

    const scraper = new Scraper(url, config);

    try {
      if ((!argv.H && !argv.e && !argv.c) || argv.t) console.log(await scraper.pageText());
      if (argv.l) console.log(await scraper.links());
      if (argv.H) console.log(await scraper.getHtml());
      if (argv.S) console.log(await scraper.scrape(argv.S));
      if (argv.e) console.log(await scraper.evaluate(argv.e));
      if (argv.c) console.log(await scraper.getCookies());
      if (argv.save) {
        const response = await scraper.getResponse();
        saveFiles(response);
      }
    } catch (error) {
      log(`:r:- :x:An error occurred: ${error}`);
    }

  })();

} else {

  const exportFunctions = async (url, Options = {}) => {

    const opts = {
      text: true,
      links: true,
      html: false,
      cookies: true,
      headless: true,
      verbose: false,
    };

    const options = { ...opts, ...Options };

    const scrapeOptions = {
      headers: options.headers || {},
      method: options.post ? 'POST' : 'GET',
      headless: options.headless,
      cookies: options.cookies,
      jar: options.jar,
    };

    if (options.file) {
      try {
        const cookies = fs.readFileSync(options.file, 'utf8');
        scrapeOptions.jar = cookies.split(';').map((cookie) => cookie.trim());
        if(options.verbose) log(`:g:+ :x:Loaded cookies from file ${options.file}`);
      } catch (error) {
        log(`:r:- :x:Failed to load cookies from file: ${options.file} - Continuing without cookies.`);
      }
    };

    const scraper = new Scraper(url, scrapeOptions);

    await scraper.ensureInitialized();

    const results = {};

    if (options.text) results.text = await scraper.pageText();
    if (options.links) results.links = await scraper.links();
    if (options.html) results.html = await scraper.getHtml();
    if (options.selector) results.scraped = await scraper.scrape(options.selector);
    if (options.eval) results.evalResult = await scraper.evaluate(options.eval);
    if (options.cookies) results.cookies = await scraper.getCookies();

    if (options.save) {
      const response = await scraper.getResponse();
      saveFiles(response);
    }

    return results;

  };

  module.exports = exportFunctions;

}


