# Automate BootStrapping Scripts

Auto, was built as a method of providing users with a simple yet extensive way to bootstrap and install Arch Linux.

During the installation process, you will be given the option to install either bare Arch Linux, and make all further installations decisions 
yourself, or to choose from a selection of one of the supported bare, or pre-configured desktop environment and window managers. 

We beleive that Arch Linux, being a rolling release distribution, should if possible always be installed via a script in opposed to an ISO
image. This will in turn always ensure that the user is getting the most recent and up to date packages, enhancing user experience, numerous 
security factors and ensuring a successfull installation.

---

The package consists of 4 scripts:

+ **auto**: A front-end to initiate the installlation process.
+ **autoPart**: A partitioning script.
+ **autoInstall**: The base install script.
+ **autoCleanInstall**: The clean install script.


`autoInstall`: This script is to be used for installing AutoLinux ontop of an existing Arch Linux installation, as a new user profile.


`autoCleanInstall`: This script is intended for use when performing a clean/fresh installation, with no pre-existing OS installed on the partition(s) reserved for the installation.
This script also includes section for partitioning your drive, so running the autoPart script is **NOT** required.


`autoPart`: This script is for useage outside of the AutoLinux installation process, and serves simply as a helper when partitioning your drive for any fresh installation.

*NOTE*: The options are limited to partition selection, or drive wipe. 
As for creating new partitions and deleting existing partitions, an external program such as cfdisk, fdisk or parted will be required if necessary.
The script also does not allow for manual selection of a file system type, and the rootfs is automatially set to ext4.



## Usage:

The `auto` script is used to initiate the installation process, and each step of the install is explained there further.\n
However, these scripts can also be run manually if the user chooses to do so.

If run manually, please read the function of both the `autoInstall` and `autoCleanInstall` script, and select *EITHER* `autoInstall` or `autoCleanInstall` to carry out 
the installation process. The `autoCleanInstall` script calls the `autoInstall` script upon completion of the base installation, so running both scripts is **NOT** required. 



## Currently Supported Editions:

+ AutoLinux - DWM Edition
+ AutoLinux - Plasma Edition
+ KDE Plasma (Plasma out the box)
+ Gnome (Gnome out the box)
+ Xfce4 (Xfce out the box)
+ Bare Arch Linux (No Additional Packages)  



**Prerequisites**

+ The script takes care of all requirements dependencies do not have to be manually installed. ✔


I hope you find these scripts helpfull !      - a4to

