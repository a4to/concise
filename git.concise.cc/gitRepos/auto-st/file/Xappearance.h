
// * Default colors (colorname index)
// * foreground, background, cursor, reverse cursor

unsigned int defaultfg = 259;
unsigned int defaultbg = 258;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
unsigned int background = 258;

// Xresources preference
// Terminal colors (16 first used in escape sequence)

static const char *colorname[] = {
  "#000000",
  "#CD0000",
  "#00CD00",
  "#CD00CD",
  "#1E90FF",
  "#CDCD00",
  "#00FFFF",
  "#FFFFFF",
  "#CD0000",
  "#CD0000",
  "#00CD00",
  "#00FFFF",
  "#45A59A",
  "#CD00CD",
  "#00FFFF",
  "#67D0A8",
  [255] = 0,
  "#add8e6",
  "#555555",
  "#1C1A1A", 
  "#67D0A8",
};

ResourcePref resources[] = {
  { "termname",     STRING,  &termname },
  { "shell",        STRING,  &shell },
  { "minlatency",   INTEGER, &minlatency },
  { "maxlatency",   INTEGER, &maxlatency },
  { "blinktimeout", INTEGER, &blinktimeout },
  { "bellvolume",   INTEGER, &bellvolume },
  { "tabspaces",    INTEGER, &tabspaces },
  { "borderpx",     INTEGER, &borderpx },
  { "cwscale",      FLOAT,   &cwscale },
  { "chscale",      FLOAT,   &chscale },
  { "alpha",        FLOAT,   &alpha },
  { "alphaOffset",  FLOAT,   &alphaOffset },
  { "color0",       STRING,  &colorname[0] },
  { "color1",       STRING,  &colorname[1] },
  { "color2",       STRING,  &colorname[2] },
  { "color3",       STRING,  &colorname[3] },
  { "color4",       STRING,  &colorname[4] },
  { "color5",       STRING,  &colorname[5] },
  { "color6",       STRING,  &colorname[6] },
  { "color7",       STRING,  &colorname[7] },
  { "color8",       STRING,  &colorname[8] },
  { "color9",       STRING,  &colorname[9] },
  { "color10",      STRING,  &colorname[10] },
  { "color11",      STRING,  &colorname[11] },
  { "color12",      STRING,  &colorname[12] },
  { "color13",      STRING,  &colorname[13] },
  { "color14",      STRING,  &colorname[14] },
  { "color15",      STRING,  &colorname[15] },
  { "background",   STRING,  &colorname[258] },
  { "foreground",   STRING,  &colorname[259] },
  { "cursorColor",  STRING,  &colorname[256] }
};

//"#010101",
//"#110C0F",
//"#131313",
//"#23181D",
//"#45313A",
//"#CE3804",
//"#FA3C00",
//"#E8781E",
//"#D12F5E",
//"#ED8B21",
//"#FEEB00",
//"#AB47BB",
//"#B94CCA",
//"#1ABADD",
//"#1393E4",
//"#B2C8DB",
//"255] = 0,
//"#add8e6",
//"#555555",
//"#1C1A1A", 
//"#67D0A8",

//"#040303",
//"#0F0B0A",
//"#2C0706",
//"#1B1C20",
//"#541314",
//"#513F4E",
//"#55747B",
//"#E2611B",
//"#AD3F42",
//"#E3B036",
//"#EEDC5E",
//"#B8118C",
//"#45A59A",
//"#67D0A8",
//"#97E4B0",
//"#CBD9A4",
//"255] = 0,
//"#add8e6",
//"#555555",
//"#1C1A1A", 
//"#67D0A8",



