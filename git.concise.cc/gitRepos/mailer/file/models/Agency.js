const mongoose = require('mongoose');

const AgencySchema = new mongoose.Schema({
  tradeName: { type: String },
  legalName: { type: String },
  domainName: { type: String },
  base64Logo: { type: String },
  funeralHomes: [{ type: String }],
  intLogoCSS: { type: String },
  extLogoCSS: { type: String },
  isAgency: { type: Boolean },
  recruiter: { type: String },
  landLogoCSS: { type: String },
  contactPerson: { type: String },
  sector: { type: String },
  contactPersonEmail: { type: String },
  preLandPage: { type: Boolean },
  link: { type: String },
  name: { type: String },
  email: { type: String },
  domain: { type: String },
  date: { type: Date, default: new Date().toLocaleString() }
});

const Agency = mongoose.model('Agency', AgencySchema);

module.exports = Agency
