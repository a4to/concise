const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const logSchema = new Schema({
  type: { type: String },
  logs: {
    type: [
      {
        type: { type: String },
        user: { type: String },
        userId: { type: String },
        info: { type: String },
        date: { type: String, default: new Date().getDate().toString().padStart(2, '0') + '/' + (new Date().getMonth() + 1 + '/' + new Date().getFullYear() + ', ' + new Date().getHours().toString().padStart(2, '0') + ':' + new Date().getMinutes().toString().padStart(2, '0') + ' ' + (new Date().getHours() > 12 ? 'PM' : 'AM')) }
      }
    ],
    default: []
  },
});

const Log = mongoose.model('Log', logSchema);

module.exports = Log;

