const mongoose = require('mongoose');

const OrderSchema = new mongoose.Schema({
  username: { type: String, default: 'concise' },
  token: { type: String, default: "pvcGl4283LOIuxHGipVL3zeBXlSbvH04KXrn9qZ4vXLBQhgdR5" },
  msisdn: { type: String },
  sim_card_number: { type: String },
  order_number: { type: String },
  reference_1: { type: String },
  reference_2: { type: String },
  suspend_behaviour: { type: Number, default: 1 },
  package_id: { type: Number, default: 30000 },
  date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Order', OrderSchema);

