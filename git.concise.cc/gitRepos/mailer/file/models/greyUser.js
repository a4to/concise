const mongoose = require('mongoose');
const ans = () => {

  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate().toString().padStart(2, '0');
  const hour = date.getHours().toString().padStart(2, '0');
  const minute = date.getMinutes();

  const ans = `${day}/${month}/${year}, ${hour}h${minute}`;
  return ans;

}

const date = ans();

const anss = () => {
  const date = new Date();
  date.setMonth(date.getMonth());
  const year = date.getFullYear();
  const month = date.toLocaleDateString('en-GB', {  month: 'long' }).substr(0, 3);
  const day = date.getDate().toString()
	return `${day} ${month} ${year}`;
}

const abdate = anss();

const loginDate = abdate + ', ' + new Date().getHours().toString().padStart(2, '0') + 'h' + new Date().getMinutes().toString().padStart(2, '0');


const UserSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  name: {
    type: String,
  },
  title: {
    type: String,
  },
  surname: {
    type: String,
  },
  invName: {
    type: String,
  },
  invSurname: {
    type: String,
  },
  invEmail: {
    type: String,
  },
  invTitle: {
    type: String,
  },
  contactno: {
    type: String,
  },
  studentno: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  photo: {
    type: String,
    default: '/img/default/user.png'
  },
  agent: {
    type: String,
    required: true,
  },
  firstLogin: {
    type: Boolean,
    default: true,
  },
  verified: {
    type: Boolean,
    default: false,
    required: true
  },
  invoiceId: {
    type: String,
    required: true,
    default: 'NA'
  },
  google: {
    type: Boolean,
    default: false
  },
  googleId: {
    type: String
  },
  googleEmail: {
    type: String
  },
  googleName: {
    type: String
  },
  emails: {
    type: String,
  },
  invoices: {
    type: [String],
  },
  date: {
    type: String,
    default: date
  },
  address: {
    type: String,
  },
  address2: {
    type: String,
  },
  city: {
    type: String,
  },
  province: {
    type: String,
  },
  postalCode: {
    type: String,
  },
  saveInfo: {
    type: Boolean,
  },
  lastLogin: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: loginDate }
      }
    }
  ],
  last2Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: loginDate }
      }
    }
  ],
  last3Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: loginDate }
      }
    }
  ],
  last4Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: loginDate }
      }
    }
  ],
  last5Login: [
    {
      type: {
        ip: { type: String, default: 'Unknown' },
        os: { type: String, default: 'Unknown' },
        version: { type: String, default: 'Unknown' },
        browser: { type: String, default: 'Unknown' },
        browserVersion: { type: String, default: 'Unknown' },
        CPU: { type: String, default: 'Unknown' },
        currentResolution: { type: String, default: 'Unknown' },
        timeZone: { type: String, default: 'Unknown' },
        language: { type: String, default: 'Unknown' },
        core: { type: String, default: 'Unknown' },
        date: { type: String, default: loginDate }
      }
    }
  ],
  lastLogins: {
    type: [String],
    default: []
  },
  resetPasswordToken: {
    type: String,
  },
  rating: {
    type: String,
    default: 'NA'
  },
  resetPasswordExpires: {
    type: Date,
  },
  success: {
    type: Boolean,
    default: false,
  },
  sims: {
    type: [
      {
        number: { type: String },
        MSISDN: { type: String },
        simId: { type: String },
        packageSize: { type: Number },
        remainingData: { type: Number },
        usageMtd: { type: Number },
        startDate: { type: String, default: abdate },
        active: { type: Boolean, default: false },
        billed: { type: String, },
        nickname: { type: String, default: 'NA' },
        currentAllocation: { type: Number },
        previousAllocation: { type: Number  },
        usageHistory: { type: Array, default: [] },
        status: { type: String }
      }
    ],
    default: []
  },
  simCount: {
    type: Number,
    default: 0
  }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
