const mongoose = require('mongoose');

const AdminSchema = new mongoose.Schema({
  username: { type: String },
  password: { type: String },
  displayname: { type: String },
  name: { type: String },
  token: { type: String },
  user: { type: String },
  invoiceCode: { type: String },
  adminPanel: { type: String },
  username: { type: String },
  password: { type: String },
  matrix: {
    type: Object,
    default: {}
  },
  code: { type: String },
  displayname: { type: String },
  agencies: {
    type: [Object],
    default: []
  },
  yoco: {
    type: Object,
    default: {}
  },
  chats: [
    {
      name: { type: String },
      msisdn: { type: String },
      messages: [Object],
    }
  ],
  whatsAdmin: { type: String },
});

const Admin = mongoose.model('Admin', AdminSchema);

module.exports = Admin
