const mongoose = require('mongoose');

const WhatsAdminSchema = new mongoose.Schema({
  admin: { type: String },
  status: { type: String },
  msisdn: { type: String },
  notes: { type: Array },
  userList: { type: Array },
  deletedUsers: { type: [Object] },
  authCheckTime: { type: Number },
  org: { type: String },
  adminId: { type: String },
});

const WhatsAdmin = mongoose.model('WhatsAdmin', WhatsAdminSchema);

module.exports = WhatsAdmin
