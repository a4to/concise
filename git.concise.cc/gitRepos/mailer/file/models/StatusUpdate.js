
const mongoose = require('mongoose');

const StatusUpdateSchema = new mongoose.Schema({
  msisdn: { type: String },
  status: { type: String },
  date: { type: Date, default: new Date().toUTCString() },
});

const StatusUpdate = mongoose.model('StatusUpdate', StatusUpdateSchema);

module.exports = StatusUpdate;

