const mongoose = require('mongoose');

const GoogleAccountSchema = new mongoose.Schema({
  googleId: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId
  },
  username: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  surname: {
    type: String,
    required: true,
  },
  photo: {
    type: String,
  },
  email: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now,
  }
})

const GoogleAccount = mongoose.model('GoogleAccount', GoogleAccountSchema);

module.exports = GoogleAccount;
