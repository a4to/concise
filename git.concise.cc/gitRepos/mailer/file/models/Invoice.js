const mongoose = require('mongoose');
const date = Date().toLocaleString();
const Schema = mongoose.Schema;

const invoiceSchema = new Schema({
  invoiceId: {
    type: String,
  },
  invName: {
    type: String,
  },
  invSurname: {
    type: String,
  },
  invEmail: {
    type: String,
  },
  discount: {
    type: Boolean,
    required: true,
    default: false
  },
  address: {
    type: String,
    default: 'N/A'
  },
  city: {
    type: String,
    default: 'N/A'
  },
  taxNo: {
    type: Number,
  },
  province: {
    type: String,
    default: 'N/A'
  },
  zip: {
    type: String,
  },
  paymentStatus: { type: String },
  paymentDate: { type: String },
  postalCode: {
    type: String,
  },
  contactno: {
    type: String,
  },
  title: {
    type: String,
  },
  date: {
    type: String,
    default: new Date().toUTCString().slice(5, -13) + ', ' + new Date().toLocaleTimeString('en-gb', { time: 'numeric' })
  },
  MSISDN: {
    type: String,
    required: true
  },
  amount: {
    type: String,
    required: true
  },
  type: {
    type: String,
  },
  kind: {
    type: String,
  },
  status: {
    type: String,
    required: true,
    default: 'Pending'
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  address2: {
    type: String,
  },
    dueDate: {
    type: String,
    required: true
  },
  billed: {
    type: String,
  },
  currentDate: {
    type: String,
    required: true
  },
  nickname: {
    type: String,
  },
  issuedBy: {
    type: String,
  },
  code: {
    type: String,
  },
});

module.exports = mongoose.model('Invoice', invoiceSchema);
