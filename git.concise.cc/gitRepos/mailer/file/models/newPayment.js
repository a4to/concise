const mongoose = require('mongoose');

const newPaymentSchema = new mongoose.Schema({
  data: [Object]
});

const newPayment = mongoose.model('newPayment', newPaymentSchema);

module.exports = newPayment;
