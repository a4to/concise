const mongoose = require('mongoose');

const PayNoticeSchema = new mongoose.Schema({
  data: [Object],
  date: { type: Date, default: new Date().toUTCString().slice(5, -13) + ', ' + new Date().toLocaleTimeString('en-gb', { time: 'numeric' }) },
});

const PayNotice = mongoose.model('PayNotice', PayNoticeSchema);

module.exports = PayNotice

