const mongoose = require('mongoose');

const ans = () => {

  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate().toString().padStart(2, '0');
  const hour = date.getHours().toString().padStart(2, '0');
  const minute = date.getMinutes();

  const ans = `${day}/${month}/${year}, ${hour}:${minute} ${hour > 12 ? 'pm' : 'am'}`;
  return ans;

}

const date = ans();

const SimSchema = new mongoose.Schema({
  MSISDN: { type: String, },
  userId: { type: String, },
  simId: { type: String, },
  name: { type: String, },
  active: { type: Boolean, default: false },
  packageSize: { type: Number, },
  billed: { type: String, },
  nickname: { type: String, },
  paymentStatus: { type: String },
  paymentDate: { type: String },
  status: { type: String, default: 'Awaiting payment verification' },
  createdAt: { type: String, default: date },
  ofTotal: { type: Number, default: 1 },
  currentAllocation: { type: Number },
  previousAllocation: { type: Number  },
  usageHistory: { type: Array, default: [] },
});

const Sim = mongoose.model('Sim', SimSchema);

module.exports = Sim;
