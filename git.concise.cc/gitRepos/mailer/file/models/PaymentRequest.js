
const mongoose = require('mongoose');

const PaymentRequestSchema = new mongoose.Schema({
  userId: { type: String },
  invoiceId: { type: String },
  status: { type: String },
  detail: { type: String },
  date: { type: Date, default: new Date().toUTCString() },
});

const PaymentRequest = mongoose.model('PaymentRequest', PaymentRequestSchema);

