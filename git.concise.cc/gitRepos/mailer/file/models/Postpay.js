
const mongoose = require('mongoose');

const PostpaySchema = new mongoose.Schema({
  transaction: { type: Object },
  msisdn: { type: String },
  site: { type: String },
  date: { type: Date, default: new Date() },
  email: { type: String },
  token: { type: String },
});

const Postpay = mongoose.model('Postpay', PostpaySchema);

module.exports = Postpay
