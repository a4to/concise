const mongoose = require('mongoose');

const emailVerificationSchema = new mongoose.Schema({
  userId: String,
  uniqueId: String,
  createdAt: Date,
  expiresAt: Date
});

const emailVerification = mongoose.model('emailVerification', emailVerificationSchema);

module.exports = emailVerification;
