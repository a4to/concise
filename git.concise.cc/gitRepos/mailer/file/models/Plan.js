const mongoose = require('mongoose');

const PlanSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  name: {
    type: mongoose.Schema.Types.ObjectId,ref: 'User',
  },
  surname: {
    type: mongoose.Schema.Types.ObjectId,ref: 'User',
  },
  email: {
    type: mongoose.Schema.Types.ObjectId,ref: 'User',
  },
  ref: {
    type: String,
  },
  size: {
    type: String,
    default: '30',
  },
  start: {
    type: Date,
    default: Date.now 
  },
});

const Plan = mongoose.model('Plan', PlanSchema);

module.exports = Plan;
